Example Applications
====================

.. toctree::
   :maxdepth: 2
   
   real_examples
   synthetic_examples
   
   
.. admonition :: Contribute

    If you have a published MARE2DEM example you'd like to showcase here,
    `contact us! <mailto:kkey@ldeo.columbia.edu>`_   
