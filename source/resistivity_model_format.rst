.. _sect_resistivity_file:

Resistivity File (.resistivity)
-------------------------------

The resistivity iteration file contains pointers to all the other files
(data, polygon, penalty, and settings files), a few options for the
inversion algorithm, and then lists the resistivity parameters for each
region.  Each inversion iteration writes out a new resistivity  file
with that iteration's results. The name convention is
`<BaseName>.<iteration# >.resistivity`.  The starting model
should be `<BaseName>.0.resistivity`.  See the example below for
further details.
 

.. _sect_model_file:

Model File (.poly)
------------------

The model file (also referred to as the poly or polygon file) 
defines the model parameter polygons using nodes and
segments.  It is assumed that the model has been built sensibly so that no two
segments intersect (except where they share a common node), that the
model is free of slivers (small angle intersections between segments,
less than about 20º), and that each segment bound region has an
identifier in the regional attributes listing. 

The model file uses the  format defined for J. Shewchuk's mesh
generation code ``Triangle.c``, which is the mesh generation code
used by MARE2DEM. The file format is:

.. code-block:: none

    First line: <# vertices> <dimension (must be 2)> <# attributes> <# boundary markers (0 or 1)>
    Following lines: <vertex #> <y> <z> [attributes][boundary marker]
    One line: <# of segments> <# of boundary markers (0 or 1)>
    Following lines: <segment #> <endpoint> <endpoint>[boundary marker]
    One line: <# of holes>
    Following lines: <hole #> <y> <z>
    Optional line: <# of regional attributes and/or area constraints>
    Optional following lines: <region #> <y> <z> <attribute><maximum area>
 
 

MARE2DEM uses the vertex locations, the segment endpoints, segment
boundary markers, and the region locations and numbers. Holes are
ignored. For the regional attributes, the  attribute value and maximum
area constraints are ignored by MARE2DEM, but the (y,z) location of each
regional attribute is used to identify the region number of segment
bound regions, and should be located at some point interior to the
region.     The segment boundary markers are used to store which
segments have penalty cuts.


More detailed information on the Poly file format can be found at the
Triangle.c website:  `http://www.cs.cmu.edu/~quake/triangle.html <http://www.cs.cmu.edu/~quake/triangle.html>`_. 

Example Resistivity and Model Files
-----------------------------------
Here's an example of the resistivity and polygon files for a layer of air over two
quarter spaces (so three regions). This is only a toy example since it
has only three parameters and uses a few of the special add-on features
of the resistivity file (bounds and prejudices).

The file ``Test.0.resistivity`` specifies the resistivity (in linear ohm-m) of each region and if it is a free
parameter. Free parameters can also have inversion bounds and prejudices.  In this example regions 1 and 3 are free
parameters so they have non-zero parameter numbers. Each has a starting value of 1 ohm-m and is bound between 0.1 to
1000 ohm-m, with a prejudiced value of 10 (and weight 1). Bounds that are 0 are ignored. Prejudices are ignored when the
weights are 0.   Region 2 is the overlying air layer, hence it is not a free parameter and has a large resistivity.  

.. code-block:: none
 

    Format:                         MARE2DEM_1.1                 ! input   
    Model File:                     Test.poly                    ! input   
    Data File:                      Test.emdata                  ! input   
    Settings File:                  mare2dem.settings            ! input     
    Maximum Iterations:             100                          ! input (optional for inversion) 
    Bounds Transform:               bandpass                     ! input (optional for inversion) 
    Global Bounds:                  1.0000E-01,   1.0000E+05     ! input (optional for inversion) 
    Roughness Penalty Method:       gradient                     ! opt. input (gradient or first_difference)  
    Roughness Weights (y,z):        3, 1                         ! opt. input (e.g. 3.0,1.0). 
    Penalty Cut Weight:             0.1                          ! opt. input (e.g. 0.1) 
    Roughness With Prejudice:       no                           ! opt. input (yes or no). Yes uses norm: || R(m-m_prej)||^2  
    Min. Gradient Support Weight:   0                            ! opt. input (e.g. 0.01) 0 means no MGS
    Print Level:                    1                            ! input (optional for inversion)  
    Target Misfit:                  1.000                        ! input (required for inversion)          
    Misfit Decrease Threshold:      0.8500                       ! input (optional for inversion)  
    Converge Slowly:                no                           ! input (required for inversion)  
    Lagrange Value:                 -0.1684E+01                  ! input/output (required for inversion) 
    Model Roughness:                74.95                        ! output from inversion 
    Model Misfit:                   1.0010                       ! output from inversion  
    Date/Time:                      01/26/2012 01:05:48          ! output from inversion 
    Anisotropy:                     isotropic
    Number of regions:  3
    !#      Rho  Param # Lower/Upper Bounds  Prejudices & Weights
    1       1        1        0.1      1000     10       1        
    2       1e+12    0        0        0        0        0        
    3       1        2        0.1      1000     10       1        
 
The file ``Test.poly``:

.. code-block:: none
 
    8 2 0 0
    1 -100000 -100000 
    2 -100000 100000 
    3 100000 100000 
    4 100000 -100000 
    5 -100000 0 
    6 100000 0 
    7 0 0 
    8 0 100000 
    10 0 
    1 4 1
    2 5 1
    3 5 2
    4 6 3
    5 6 4 
    6 7 5
    7 7 6
    8 8 2
    9 8 3
    10 8 7
    0
    3
    1 -70710.7 29289.3 1 -1 
    2 -70710.7 -29289.3 2 -1 
    3 -29289.3 70710.7 3 -1 

 
There are several other features of the resistivity file that are not
described here, and the format expands to handle various types of
anisotropy MARE2DEM is capable of modeling.
