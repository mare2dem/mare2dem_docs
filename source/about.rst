
About
*****

*MARE2DEM: Modeling with Adaptively Refined Elements for 2D Electromagnetics*


**MARE2DEM** (pronounced *mahr-ey 2DEM*) is a parallel adaptive finite
element code for 2D forward and inverse modeling for electromagnetic
geophysics. Initially developed with funding support from the
`Scripps Seafloor Electromagnetic Methods Consortium
<https://marineemlab.ucsd.edu/semc.html>`_, it is now supported by the
`Electromagnetic Methods Research Consortium at Columbia University
<http://emrc.ldeo.columbia.edu>`_. New features are made exclusively
available to consortium sponsors for a period of time and later are
released publicly.  

MARE2DEM works for 2D anisotropic modeling for controlled-source
electromagnetic (CSEM), magnetotelluric (MT) and surface-borehole EM
applications in onshore, offshore and downhole environments.  The main
benefits of MARE2DEM are:

- Fully automatic unstructured mesh generation so that end-users 
  are free from the burden of designing numerically accurate grids for complicated models.
- Interactive graphical user-interface for designing models and viewing results.
- Open-source license and freely available.

  
General Features and Capabilities
---------------------------------

- Forward calculations using fully automatic goal-oriented adaptive finite elements
- Non-linear inversion using a fast parallel implementation of Occam’s method,
  a regularized Gauss-Newton minimization technique
- Magnetotelluric (MT) plane waves using total field or scattered field implementations
- Frequency domain marine, land and borehole 2.5D EM modeling (2.5D means 3D source field in 2D geometry)
- Arbitrary locations and rotations for receivers and transmitters
- Support for point or finite length dipole wires for both transmitters and receivers
- Efficient inversion of long lines of towed-streamer marine EM data using moving footprint windows 
- Support for isotropic, transversely isotropic, triaxial anisotropic or isotropic complex conductivity
- Inverted conductivity parameters can be bounded using non-linear transforms
- Parallel data decomposition for a nearly linear speedup with the number of processors. 
- Dense matrix operations during inversion are efficiently handled in
  parallel using the ScaLAPACK library
- Can be run in parallel on 1000's of processors for efficient inversion of large CSEM data sets
- Can be run on laptops for MT or lightweight CSEM applications.
- MATLAB Model builder interface (Mamba2D) for building forward models with arbitrarily
  complex 2D structures, and inversion parameter grids with unstructured triangular 
  meshes or conforming quadrilateral grids. Also supports nested gridding (e.g. for 
  near-surface small scale features).
- MATLAB 2D model plotting routines allow for overlaying geologic layers, seismic reflection images
  and well logs, plotting normalized inversion sensitivity, etc.

Citation
-------- 

If you publish results made using MARE2DEM, please consider citing [Key16]_:

   Key, K., 2016, MARE2DEM: a 2-D inversion code for controlled-source 
   electromagnetic and magnetotelluric data. Geophysical Journal International, 
   207(1), 571–588. DOI: `10.1093/gji/ggw290 <https://doi.org/10.1093/gji/ggw290>`_. 

You might also consider citing some of the earlier MARE2D papers:
[KeOv11]_, [LiKe07]_, [KeWe06]_.

Developers
----------

    - **Kerry Key**,
      Lamont-Doherty Earth Observatory, Columbia University
      `http://emlab.ldeo.columbia.edu <http://emlab.ldeo.columbia.edu>`_

Interested in contributing? `Contact us! <mailto:kkey@ldeo.columbia.edu>`_

License 
------- 

MARE2DEM is made freely available under the `GNU GPLv3 license 
<https://www.gnu.org/licenses/gpl-3.0.en.html>`_. 

 
Acknowledgments
---------------
The following people are thanked for helpful discussions or for directly contributing to the package of MARE2DEM codes:
David Alumbaugh, Steven Constable, Mike Hoversten, David Myer, Jeff Ovall and Shunguo Wang. I also extend gratitude to
Chet Weiss, who first taught me about finite element methods during his visiting appointment at Scripps in 2003, and to
Yuguo Li, who came to Scripps as a Postdoc in 2004 and showed me that the 2.5D CSEM problem isn't so bad, despite a
heinously complicated set of coupled wavenumber domain differential equations. These collaborations resulted in
publications and early adaptive finite element forward codes named **MARE2DMT** [KeWe06]_ and **MARE2DCSEM** [LiKe07]_.
Dieter Werthmüller is thanked for all his direct and indirect teachings about the open-source ecosystem and making his
EM codes freely available. Finally, Steve Constable is thanked for many useful discussions about  Occam's inversion and
for making the original **Occam** codes (1D and 2D MT) freely available. The public release of MARE2DEM is an attempt to
follow in these footsteps. 

MARE2DEM uses the following code packages and their authors are greatly acknowledged for making these codes freely
available:

- **Triangle**
- **SuperLU**
- **LAPACK**
- **ScaLAPACK**
- **kdtree2**
 


