.. _alongstrike_warning:

Numerical Issues for Along-Strike Source-Receiver Offset
----------------------------------------------------------------------------------------     
For 2.5D EM modeling, MARE2DEM uses a Fourier transformation of the governing Maxwell's
equations along the model strike (*x* direction). This poses some potential numerical 
issues for receivers positioned too close to transmitter in the *(y,z)* plane, even if the
*x* offset is large. The figure below highlights these issues graphically.
 
.. figure::  _static/images/rx_geometry4.png 
    :width: 100%

    Safe locations for receiver relative to the transmitter in 2.5D modeling.  Due to the 
    source singularity, receivers in the red region that are close the transmitter in any 
    direction might not be accurately modeled.  
    For marine CSEM at least 100 m is generally safe.  Receivers in the pink region are close
    to the transmitter in the wavenumber domain  due to the 2.5 Fourier transformation along *x*. 
    Receivers in the grey regions have a large   :math:`| x_{tx} - x_{rx} |` that may require
    an increased range and density of the wavenumber samples to get a numerically accurate Fourier
    transformation due to severe oscillations in the transform kernel at large offsets. The precise distances 
    for these potentially problematic regions depends on the specific conductivity model, frequency 
    and kx wavenumbers sampling used; if you desire to model receivers close to the source or far
    downstrike, we recommend you verify the MARE2DEM responses for a relevant layered model with
    solutions from a numerically accurate 1D EM codes for your specific problem.
    
