
.. _sect_macos_openmpi: 

Installing OpenMPI on Apple macOS
---------------------------------
Here's some brief notes to install OpenMPI on a current (2020) Apple system running macOS Catalina 10.15.

1. **Install Apple Xcode**.  You can download Xcode at `https://developer.apple.com/xcode/ <https://developer.apple.com/xcode/>`_.
2. **Install Xcode command line tools**. Input the following command string in Terminal: 

    .. code-block:: bash
         
         xcode-select --install 
     
3.  **Install the Intel C and Fortran compilers**, including the Math Kernel
    Library.  After you install them, make sure you add the line ``source
    /opt/intel/bin/compilervars.sh intel64`` to your ``.profile`` or
    ``.bash_profile`` file in your home directory so that the terminal will know
    where the compilers are located at.  See the Intel compiler installation help  website for more details
    on that.
    
4.  **Download and install Open-MPI**.  You can obtain Open-MPI source code
    from `https://www.open-mpi.org/software/ompi/v4.0/ <https://www.open-mpi.org/software/ompi/v4.0/>`_.  
    Unpack or unzip the files. Open Terminal.app and ``cd`` to the
    open-mpi folder. Run the configure script by pasting the following into
    the terminal (this should be one line of text):
  
    .. code-block:: bash

            sudo  ./configure --prefix=/opt/openmpi CC=icc CXX=icc F77=ifort FC=ifort \
            lt_prog_compiler_wl_FC='-Wl,'; 
            make all install   
        
    Then go do something else for the annoying hour or more that it
    takes to build MPI. When this is done, you will have the mpi
    compilers and libraries installed into ``/opt/openmpi``.

.. Warning::
    If you update to a new version of macOS or a new version of Xcode,
    you will likely have to reinstall the Intel compilers and rebuild
    OpenMPI, as the compilers depend on libraries contained in Xcode. I
    **strongly** recommend moving any previously installed version of
    OpenMPI to a different folder so that you get a clean build (i.e.
    run ``sudo mv /opt/openmpi /opt/openmpi_old`` before builing the new
    openmpi), as in the past there have been file conflict issues from
    lingering deprecated files in the openmpi library.