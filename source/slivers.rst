.. _sect_slivers:

Avoid Model Slivers
===================

A sliver is where two intersecting model segments meet at an angle less
than 25º. The current triangulation engine underneath the hood of MARE2DEM does
not like slivers in the input model, so MARE2DEM will quit on startup if
it finds a sliver in your model. It will print a list of all the sliver
locations so that you can then remove them by hand in Mamba2D, which 
admittedly can be quite tedious. Hence, use care when designing models
so that you don't inadvertently create slivers.  
Slivers often occur in coastline
models where the sea intersects the
shore. They can also occur when lots of closely spaced and intersecting 
seismic  surfaces are imported.
The can also often pop up for hand drawn model structure, so try using the
“axis equal” zoom option in Mamba2D to help alleviate this.

The image on the left below shows an example sliver and 
the image on the right shows one way to de-sliver the mesh, simply by
truncating the pinch out. 

|fig1| |fig2|

.. |fig1| image:: _static/images/sliver.png
    :width: 49%

.. |fig2| image:: _static/images/no_sliver.png
    :width: 49%
        
 