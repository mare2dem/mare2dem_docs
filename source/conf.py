# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import time

# -- Project information -----------------------------------------------------

project = 'MARE2DEM Documentation'
copyright = u'2015-{}, The MARE2DEM developers'.format(time.strftime("%Y"))
author = 'The MARE2DEM developers'
version = 'v5.1.2'
release = 'v5.1.2'


 

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
'sphinxcontrib.gist',
"sphinx_multiversion",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

html_sidebars = {
    '**': [
    ],
}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme' # 'alabaster'

 
html_theme_options = {
    'logo_only': True,
    'display_version': False,
    'prev_next_buttons_location': 'both',
}

html_show_sourcelink = True

# global numbering for Figures and labelled equations across pages:
numfig = True
math_numfig = True

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".

html_static_path = ['_static']
html_logo = '_static/images/logo1.png'
html_favicon = '_static/images/favicon.png'

# These paths are either relative to html_static_path
# or fully qualified paths (eg. https://...)
html_css_files = [
    'css/custom.css',
]

# -- Options for LATEX output:
latex_logo = '_static/images/logo1.png' 

# sphinx-multiversion settings:
#  
# Whitelist pattern for tags (set to None to ignore all tags)
#smv_tag_whitelist = r'^.*$'

# Whitelist pattern for branches (set to None to ignore all branches)
#smv_branch_whitelist = r'^.*$'

# Whitelist pattern for remotes (set to None to use local branches only)
#smv_remote_whitelist = None

# Pattern for released versions
smv_released_pattern = r'^refs/tags/.*$'  

# Format for versioned output directories inside the build directory
# smv_outputdir_format = '{ref.name}'

# Determines whether remote or local git branches/tags are preferred if their output dirs conflict
# smv_prefer_remote_refs = False