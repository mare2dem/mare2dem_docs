
.. _sect_references:

References
##########

.. _references:

.. [Key16] Key, K., 2016, MARE2DEM: a 2-D inversion code for controlled-source 
   electromagnetic and magnetotelluric data. Geophysical Journal International, 
   207(1), 571–588. DOI: `10.1093/gji/ggw290 <https://doi.org/10.1093/gji/ggw290>`_.
.. [WCK15] Wheelock, B., S. Constable, and K. Key, 2015, The advantages of
    logarithmically scaled data for electromagnetic inversion. Geophysical
    Journal International, 201(3), 1765–1780. DOI: `10.1093/gji/ggv107  
    <http://doi.org/10.1093/gji/ggv107>`_.   
.. [Key12] Key, K., 2012, Is the fast Hankel transform faster than quadrature?
   Geophysics, 77(3), F21–F30; DOI: `10.1190/geo2011-0237.1
   <https://doi.org/10.1190/geo2011-0237.1>`_; Software:
   `software.seg.org/2012/0003 <https://software.seg.org/2012/0003>`_.  
.. [BKS12] Brown, V., K. Key, & S. Singh, 2012,  Seismically regularized
   controlled-source electromagnetic inversion. Geophysics, 77, E57–E65; DOI: `10.1190/GEO2011-0081.1
   <https://doi.org/10.1190/GEO2011-0081.1>`_.     
.. [KeOv11] Key, K. and J. Ovall, 2011, A parallel goal-oriented adaptive 
   finite element method for 2.5-D electromagnetic modelling. Geophysical Journal 
   International, 186(1), 137–154; DOI: `10.1111/j.1365-246X.2011.05025.x 
   <https://doi.org/10.1111/j.1365-246X.2011.05025.x>`_.       
.. [Key09] Key, K., 2009, 1D inversion of multicomponent, multifrequency marine
   CSEM data: Methodology and synthetic studies for resolving thin resistive
   layers. Geophysics, 74(2), F9–F20; DOI: `10.1190/1.3058434
   <https://doi.org/10.1190/1.3058434>`_. Software:
   `marineemlab.ucsd.edu/Projects/Occam/1DCSEM
   <https://marineemlab.ucsd.edu/Projects/Occam/1DCSEM>`_.   
.. [LiKe07] Li, Y., and K. Key, 2007, 2D marine controlled-source electromagnetic 
   modeling: Part 1–An adaptive finite element algorithm. Geophysics, 72(2), 
   WA51–WA62; DOI: `10.1190/1.2432262 <https://doi.org/10.1190/1.2432262>`_.
.. [KeWe06] Key, K. and C. Weiss, 2006, Adaptive finite-element modeling 
   using unstructured grids: The 2D magnetotelluric example. Geophysics, 
   71(6), G291–G299. DOI: `10.1190/1.2348091 <http://doi.org/10.1190/1.2348091>`_.      
.. [PoZh99] Portniaguine, O. and M.S. Zhdanov, 1999, Focusing geophysical
    inversion images. Geophysics, 64, 874–887. DOI: 
    `10.1190/1.1444596<https://doi.org/10.1190/1.1444596>``

  