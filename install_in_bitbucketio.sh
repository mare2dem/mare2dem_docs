#! /bin/zsh
 
# this script could build and install both html and pdf:
echo "cleaning previous build..."

make clean # just in case, start from a clean slate

rm -fR ../mare2dem.bitbucket.io/*

# Build latex pdf:
#make latexpdf

# copy pdf to ../mare2dem.bitbucket.io/:
#cp -R ./_build/latex/mare2dem.pdf ../mare2dem.bitbucket.io/

# build website:
#sphinx-multiversion  --dump-metadata source _build/html
sphinx-multiversion source _build/html

# make redirect to main:
cat >> _build/html/index.html <<'EOF'
<!DOCTYPE html>
<html>
  <head>
    <title>Redirecting to main branch</title>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="0; url=./master/index.html">
    <link rel="canonical" href="/index.htmlhttps://mare2dem.bitbucket.io">
  </head>
</html> 

EOF

# install in bitbucket repo local copy:
echo "copying  ./_build/html/ to ../mare2dem.bitbucket.io/..."
cp -R ./_build/html/ ../mare2dem.bitbucket.io/

# commit and push to bitbucket:

# to do!



